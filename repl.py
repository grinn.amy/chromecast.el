### repl.py --- Python read-eval-print loop

## Copyright (C) 2024 Amy Grinn

### Commentary

## Launches a minimal REPL, without a prompt.

import code

class Repl(code.InteractiveInterpreter):
    def interact(self):
        while True:
            try:
                self.runcode(input())
            except Exception as e:
                print(f'Error: {e}')

repl = Repl()
repl.interact()
