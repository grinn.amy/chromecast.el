;;; chromecast.el --- Control your chromecast -*- lexical-binding: t -*-

;; Copyright (C) 2024 Amy Grinn

;; Author: Amy Grinn <grinn.amy@gmail.com>
;; Version: 2.0.0
;; File: chromecast.el
;; Package-Requires: ((emacs "27.1") (transient "0.5"))
;; Keywords: tools
;; URL: https://gitlab.com/grinn.amy/chromecast.el

;;; Commentary:

;; chromecast.el provides commands for interacting with a chromecast.
;;
;; Requires the python package `pychromecast' which can be found in
;; apt and pip.
;;
;; Usage:
;;
;; `chromecast': Select a device (if not already selected) and control
;; it through a transient interface.

;;; Code:

;;;; Requirements:

(require 'transient)

;;;; Options

(defgroup chromecast nil
  "Customization options for chromecast.el."
  :group 'applications)

(defcustom chromecast-prev-max-seconds 3
  "Max seconds into the current track before going to previous."
  :type 'number)

(defcustom chromecast-default-volume-delta 10
  "Percentage delta to change the volume by."
  :type 'number)

(defcustom chromecast-default-seek-step 30
  "Number of seconds to fast-forward and reverse, by default."
  :type 'number)

(defcustom chromecast-value-max-len 16
  "Max number of characters for any device metadata."
  :type 'number)

;;;; Variables

(defconst chromecast--base (file-name-directory load-file-name)
  "Directory with the source code for chromecast.el.")

(defvar chromecast--python3 nil
  "The python3 process to use.")

(defvar chromecast--devices nil
  "A list of available devices.")

(defvar chromecast--selected nil
  "A plist describing the current device.")

(defvar chromecast--status nil
  "A plist describing the current status of `chromecast--selected'.")

(defvar chromecast--media-status nil
  "A plist describing the current media playing.")

(defvar chromecast--current-volume-delta nil
  "The current step size when changing the volume.")

(defvar chromecast--current-seek-step nil
  "The current step size when rewinding or fast-forwarding.")

;;;; Compile time setup

;; Remove existing python3 process, which might have loaded an old
;; version of api.py
(eval-when-compile
  (defvar chromecast--python3 nil)
  (when chromecast--python3
    (delete-process chromecast--python3)))

;;;; Utility functions

(defun chromecast--python3-sentinel (proc _)
  "If the process PROC stops running, remove the selected device."
  (when (not (eq 'run (process-status proc)))
    (put 'chromecast--selected 'last (plist-get chromecast--selected :name))
    (setq chromecast--selected nil)))

(defun chromecast--python3-filter (_ data)
  "Parse json DATA and update global state.

Depending on the message type, either `chromecast--devices',
`chromecast--status', or `chromecast--media-status' will be
updated.  If the `chromecast' transient is being displayed, it
will be refreshed."
  (when (string-match "@json:\\(.*\\)$" data)
    (let* ((json (json-parse-string (match-string 1 data)
                                    :object-type 'plist
                                    :array-type 'list
                                    :null-object nil
                                    :false-object nil))
           (type (plist-get json :type))
           (data (plist-get json :data)))
      (cond
       ((string= type "device")
        (push data chromecast--devices))
       ((string= type "device_status")
        (setq chromecast--status data))
       ((string= type "media_status")
        (setq chromecast--media-status data))))
    ;; TODO: implement a proper way to update a transient
    ;; Should be something like (transient-refresh 'chromecast)
    (when (and transient--prefix
               (eq 'chromecast (oref transient--prefix command)))
      (transient--refresh-transient))))

(defun chromecast--ensurepython ()
  "Ensure the process `chromecast--python3' exists and is valid."
  (when (or (not chromecast--python3)
            (not (eq 'run (process-status chromecast--python3))))
    (setq chromecast--python3
          (start-process "chromecast-python3" "*Chromecast*" "python3"
                         (expand-file-name "repl.py" chromecast--base)))
    (set-process-query-on-exit-flag chromecast--python3 nil)
    (set-process-sentinel chromecast--python3 'chromecast--python3-sentinel)
    (add-function :after (process-filter chromecast--python3)
                  'chromecast--python3-filter)
    (process-send-string chromecast--python3 "from api import *\n")))

(defun chromecast--cmd (cmd &rest args)
  "Send CMD to the selected chromecast device.

ARGS will be passed to CMD in the python3 process."
  (process-send-string chromecast--python3
                       (concat cmd "(" (string-join args ",") ")\n")))

(defun chromecast--select-device (&optional initial)
  "Use `completing-read' to select a chromecast-enabled device.

INITIAL will be used as the initial input for `completing-read'."
  (setq chromecast--devices nil)
  (chromecast--cmd "discover")
  (unwind-protect
      (let ((name (completing-read
                   "Device: "
                   (completion-table-dynamic
                    (lambda (_) (mapcar
                                 (lambda (device) (plist-get device :name))
                                 chromecast--devices)))
                   nil t initial)))
        (setq chromecast--selected
              (seq-find
               (lambda (device) (string= (plist-get device :name) name))
               chromecast--devices)))
    (chromecast--cmd
     "select"
     (when chromecast--selected
       (format "\"%s\"" (plist-get chromecast--selected :uuid))))))

;;;; Macros

(defmacro chromecast--interactive (&rest body)
  "Define an interactive function with a device selected.

Run BODY after ensuring that a chromecast device is selected.  It
should return a list of arguments to pass to the resulting
function."
  `(interactive
    (progn
      (chromecast--ensurepython)
      (unless chromecast--selected
        (chromecast--select-device (get 'chromecast--selected 'last)))
      nil
      ,@body)))

(defmacro chromecast--opt (prompt &optional initial)
  "Define optional string parameter to an interactive function.

PROMPT and INITIAL will be used, if the prefix argument is equal
to (4), to read the string value from the user."
  `(list (if (equal current-prefix-arg '(4))
             (read-string ,prompt ,initial)
           (if current-prefix-arg
               (number-to-string current-prefix-arg)
             nil))))

;;;; Commands

;;;###autoload
(defun chromecast-select ()
  "Select a chromecast device."
  (interactive)
  (chromecast--ensurepython)
  (chromecast--select-device))

(defun chromecast-refresh-status ()
  "Refresh the status of the currently selected device."
  (chromecast--interactive)
  (chromecast--cmd "print_status"))

;;;###autoload
(defun chromecast-quit ()
  "Stop the currently-running app on a chromecast device."
  (chromecast--interactive)
  (when (y-or-n-p (format "Quit running %s on %s?"
                          (plist-get chromecast--status :app)
                          (plist-get chromecast--selected :name)))
    (chromecast--cmd "quit_app")))

;;;###autoload
(defun chromecast-volume-up (&optional delta)
  "Raise the volume of the selected chromecast device by DELTA."
  (chromecast--interactive
   (chromecast--opt "Delta (between 0 and 1): " "0."))
  (chromecast--cmd
   "volume_up"
   (or delta
       (alist-get 'delta (transient-args 'chromecast))
       (number-to-string chromecast-default-volume-delta))))

;;;###autoload
(defun chromecast-volume-down (&optional delta)
  "Lower the volume of the selected chromecast device by DELTA."
  (chromecast--interactive
   (chromecast--opt "Delta (between 0 and 1): " "0."))
  (chromecast--cmd
   "volume_down"
   (or delta
       (alist-get 'delta (transient-args 'chromecast))
       (number-to-string chromecast-default-volume-delta))))

;;;###autoload
(defun chromecast-toggle-muted ()
  "Toggle whether the chromecast device is muted."
  (chromecast--interactive)
  (chromecast--cmd "toggle_muted"))

;;;###autoload
(defun chromecast-toggle-paused ()
  "Unpause the media playing on the chromecast device."
  (chromecast--interactive)
  (chromecast--cmd "toggle_paused"))

;;;###autoload
(defun chromecast-rewind (&optional seconds)
  "Rewind the current media SECONDS seconds."
  (chromecast--interactive
   (chromecast--opt "Seconds: "))
  (chromecast--cmd
   "rewind"
   (or seconds
       (alist-get 'step (transient-args 'chromecast))
       (number-to-string chromecast-default-seek-step))))

;;;###autoload
(defun chromecast-fast-forward (&optional seconds)
  "Fast-forward the current media SECONDS seconds."
  (chromecast--interactive
   (chromecast--opt "Seconds: "))
  (chromecast--cmd
   "fast_forward"
   (or seconds
       (alist-get 'step (transient-args 'chromecast))
       (number-to-string chromecast-default-seek-step))))

;;;###autoload
(defun chromecast-next ()
  "Skip to the next track."
  (chromecast--interactive)
  (chromecast--cmd "next"))

;;;###autoload
(defun chromecast-prev ()
  "Rewind or go to the previous track."
  (chromecast--interactive)
  (chromecast--cmd
   "prev"
   (number-to-string chromecast-prev-max-seconds)))

;;;; Status helpers

(defun chromecast--get-name ()
  "Get the name of the currently-selected chromecast device."
  (if-let ((name (plist-get chromecast--selected :name)))
      (truncate-string-to-width name chromecast-value-max-len nil nil t)
    ""))

(defun chromecast--get-volume ()
  "Get a string describing the volume of the selected device."
  (if (plist-get chromecast--status :muted)
      "<muted>"
    (format "%d%%" (round (* 100 (or (plist-get chromecast--status :volume)
                                     0))))))

(defun chromecast--get-mute ()
  "String which is the opposite of whether the device is muted."
  (if (plist-get chromecast--status :muted)
      "Unmute"
    "Mute"))

(defun chromecast--has-app ()
  "Return the application name if it is not the default."
  (if-let ((app (plist-get chromecast--status :app)))
      (if (not (string= app "Backdrop"))
          app)))
  
(defun chromecast--get-app ()
  "Get the current app running on the chromecast device."
  (if-let ((app (chromecast--has-app)))
      (truncate-string-to-width app chromecast-value-max-len nil nil t)
    ""))

(defun chromecast--get-media ()
  "Get the title of the currently running media on the device."
  (if-let ((title (plist-get
                   (plist-get chromecast--media-status :metadata) :title)))
      (truncate-string-to-width title chromecast-value-max-len nil nil t)
    ""))

(defun chromecast--get-paused ()
  "Get a string describing whether the device is pauased."
  (if (plist-get chromecast--media-status :paused)
      "Play"
    "Pause"))

;;;; Transient

(defclass chromecast--transient-selection (transient-infix)
  ((format :initform " %k %d %v"))
  "Chromecast device selection infix class.")

(cl-defmethod transient-infix-set ((_ chromecast--transient-selection) value)
  "Set selected device, in both the Emacs and Python environments.

VALUE should be the name of the device to select."
  (setq chromecast--selected
        (seq-find
         (lambda (device) (string= (plist-get device :name) value))
         chromecast--devices))
  (chromecast--cmd
   "select"
   (when chromecast--selected
     (format "\"%s\"" (plist-get chromecast--selected :uuid)))))

(cl-defmethod transient-format-value ((_ chromecast--transient-selection))
  "Format name of the selected device, or return an empty string."
  (if chromecast--selected
      (propertize (plist-get chromecast--selected :name)
                  'face 'transient-value)
    ""))

(transient-define-infix chromecast--transient-select ()
  "Select a chromecast-enabled device on the local network."
  :class 'chromecast--transient-selection
  :prompt "Device: "
  :argument ""
  :choices (lambda ()
             (setq chromecast--devices nil)
             (chromecast--cmd "discover")
             (completion-table-dynamic
              (lambda (_)
                (mapcar
                 (lambda (device) (plist-get device :name))
                 chromecast--devices)))))

(defclass chromecast--transient-option (transient-infix)
  ((format :initform " %k %d %v")
   (symbol :initarg :symbol)
   (var :initarg :var)
   (units :initarg :units))
  "Class for string options backed by a variable.

Along with storing the state of this option inside the transient,
the state will also be stored in VAR.  Its value will be
displayed followed by UNITS.")

(cl-defmethod transient-infix-set ((obj chromecast--transient-option) value)
  "Set the symbol in OBJ to VALUE."
  (oset obj value value)
  (eval `(setq ,(oref obj var) ,value)))

(cl-defmethod transient-infix-value ((obj chromecast--transient-option))
  "Get the current value of the OBJ's symbol slot."
  (with-slots (symbol var value) obj
    (cons symbol (if (not value)
                     (setq value (eval var))
                   value))))

(cl-defmethod transient-format-value ((obj chromecast--transient-option))
  "Format the value of OBJ, or return an empty string."
  (with-slots (symbol units) obj
    (if-let ((value (cdr (transient-infix-value obj))))
        (propertize (concat value units) 'face 'transient-value)
      "")))

(transient-define-infix chromecast--volume-delta-select ()
  "Select the delta to use when changing volume."
  :class 'chromecast--transient-option
  :var 'chromecast--current-volume-delta
  :symbol 'delta
  :prompt "Delta: "
  :units "%")

(transient-define-infix chromecast--seek-step-select ()
  "Select the delta to use when changing volume."
  :class 'chromecast--transient-option
  :var 'chromecast--current-seek-step
  :symbol 'step
  :prompt "Step size: "
  :units "s")

;;;###autoload (autoload 'chromecast "chromecast" nil t)
(transient-define-prefix chromecast ()
  "Control a chromecast-enabled device on this network."
  :transient-non-suffix 'transient-quit-one
  [["Device"
    ("s" "Select" chromecast--transient-select)
    ("q" (lambda () (concat "Quit "
                            (propertize (chromecast--get-app)
                                        'face 'transient-value)))
     chromecast-quit
     :if chromecast--has-app)
    ("g" "Refresh" chromecast-refresh-status :transient t)]
   [:description
    (lambda () (concat (propertize "Volume " 'face 'transient-heading)
                       (propertize (chromecast--get-volume)
                                   'face 'transient-value)))
    ("u" "Up" chromecast-volume-up :transient t)
    ("d" "Down" chromecast-volume-down :transient t)
    ("m" (lambda () (chromecast--get-mute))
     chromecast-toggle-muted :transient t)]
   [:description
    (lambda () (concat (propertize "Control " 'face 'transient-heading)
                       (propertize (chromecast--get-media) 'face 'transient-value)))
    :if chromecast--has-app
    ("<SPC>" (lambda () (chromecast--get-paused))
     chromecast-toggle-paused :transient t)
    ("b" "Rewind" chromecast-rewind :transient t)
    ("f" "Fast-forward" chromecast-fast-forward :transient t)
    (">" "Next" chromecast-next :transient t)
    ("<" "Prev" chromecast-prev :transient t)]
   ["Options"
    ("-d" "Volume delta" chromecast--volume-delta-select)
    ("-s" "Seek step" chromecast--seek-step-select
     :if chromecast--has-app)]]
  (chromecast--interactive)
  (transient-setup 'chromecast)
  (chromecast--cmd "print_status"))

(provide 'chromecast)
;;; chromecast.el ends here
