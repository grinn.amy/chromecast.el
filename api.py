### api.py --- Bindings for pychromecast

## Copyright (C) 2024 Amy Grinn

### Commentary

## Provides functions for interacting with a chromecast device

import sys
import json
import uuid
import zeroconf
import pychromecast

def _print_message(message_type, obj):
    sys.stdout.flush()
    sys.stdout.write("@json:")
    json.dump({ 'type': message_type, 'data': obj }, sys.stdout)
    print()

cast_listener = pychromecast.SimpleCastListener(
    lambda uuid, _: _print_message('device', {
        'name': browser.devices[uuid].friendly_name,
        'uuid': str(uuid),
    }))

def print_status():
    def device_cb(_):
        _print_message('device_status', {
            'muted': device.status.volume_muted,
            'volume': device.status.volume_level,
            'app': device.status.display_name,
        })

        if device.status.app_id:
            def media_cb(_):
                _print_message('media_status', {
                    'paused': device.media_controller.status.player_is_paused,
                    'playing': device.media_controller.status.player_is_playing,
                    'metadata': device.media_controller.status.media_metadata,
                })
            device.media_controller.update_status(media_cb)
        else:
            _print_message('media_status', {
                'paused': False,
                'playing': False,
                'metadata': None,
            })
    device.socket_client.receiver_controller.update_status(device_cb)

def discover():
    global browser
    browser = pychromecast.CastBrowser(cast_listener, zeroconf.Zeroconf())
    browser.start_discovery()

def select(id = None):
    global device
    browser.stop_discovery()
    if id:
        device = pychromecast.get_chromecast_from_cast_info(
            browser.devices[uuid.UUID(id)], zeroconf.Zeroconf())
        device.wait()
        print_status()

def quit_app():
    device.quit_app()
    print_status()

def volume_up(delta=10):
    device.volume_up(delta / 100.0)
    print_status()

def volume_down(delta=10):
    device.volume_down(delta / 100.0)
    print_status()

def toggle_muted():
    def cb(_):
        if device.status.volume_muted:
            device.set_volume_muted(False)
        else:
            device.set_volume_muted(True)
        print_status()
    device.socket_client.receiver_controller.update_status(cb)

def toggle_paused():
    def cb(_):
        if device.media_controller.status.player_is_paused:
            device.media_controller.play()
        else:
            device.media_controller.pause()
        print_status()
    device.media_controller.update_status(cb)

def rewind(seconds):
    def cb(_):
        device.media_controller.seek(
            device.media_controller.status.current_time
            - seconds)
        print_status()
    device.media_controller.update_status(cb)

def fast_forward(seconds):
    def cb(_):
        device.media_controller.seek(
            device.media_controller.status.current_time
            + seconds)
        print_status()
    device.media_controller.update_status(cb)

def next():
    device.media_controller.queue_next()
    print_status()

def prev(max_seconds=3):
    def cb(_):
        if device.media_controller.status.current_time < max_seconds:
            device.media_controller.queue_prev()
        else:
            device.media_controller.rewind()
        print_status()
    device.media_controller.update_status(cb)
